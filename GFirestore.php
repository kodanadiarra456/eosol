<?php
    require_once 'vendor/autoload.php';
    use Google\Cloud\Firestore\FirestoreClient;

    class GFirestore {
        protected $db;
        protected $name;

        public function __construct(string $collection){
            $this->db = new FirestoreClient([
                'projectId' => 'myeosol-eff2a'
            ]);

            $this->name = $collection;
        }

        public function newDocument(string $name, array $data = []){
            try {
                $this->db->collection($this->name)->document($name)->create($data);
                return true;
            } catch (Exception $exception){
                return $exception->getMessage();
            }
        }
    }

//     rules_version = '2';
// service cloud.firestore {
//   match /databases/{database}/documents {
//     match /{document=**} {
//       allow read, write: if request.auth != null;
//     }
//   }
// }