<?php

    require_once 'GFirestore.php';

    function genererChaineAleatoire($longueur = 10){
        $caracteres = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $longueurMax = strlen($caracteres);
        $chaineAleatoire = '';
        for ($i = 0; $i < $longueur; $i++)
        {
        $chaineAleatoire .= $caracteres[rand(0, $longueurMax - 1)];
        }
        return $chaineAleatoire;
    }

    $assistance = new GFirestore('assistance');

    $name = $_POST['name'];
    $email = $_POST['email'];
    $subject = $_POST['subject'];
    $message = $_POST['message'];

    $assistance->newDocument(genererChaineAleatoire(20), [
        'Name' => $name,
        'Email' => $email,
        'Sujet' => $subject,
        'Message' => $message,
    ]);

    header('Location: index.php?success=true#contact');